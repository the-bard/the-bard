const GAME_WIDTH = 800
const GAME_HEIGHT = 600

const PLAYER_WIDTH = 70
const PLAYER_HEIGHT = 128

const PLAYER_SPAWN_X = GAME_WIDTH / 2 - PLAYER_WIDTH / 2
const PLAYER_SPAWN_Y = GAME_HEIGHT / 2 - PLAYER_HEIGHT / 2

const PLAYER_SPEED = 200

var game = new Phaser.Game(GAME_WIDTH, GAME_HEIGHT, Phaser.AUTO,
  'game', { preload: preload, create: create, update: update })

function preload() {
  game.load.spritesheet('player', '../images/player.png',
    PLAYER_WIDTH, PLAYER_HEIGHT)
}

var player
var cursors

function create() {

  game.world.bounds.setTo(0, 0, GAME_WIDTH, GAME_HEIGHT)

  game.physics.startSystem(Phaser.Physics.ARCADE)

  player = new Player(game, PLAYER_SPAWN_X, PLAYER_SPAWN_Y)
  game.add.existing(player)
  player.animations.add('left', [0, 1, 2, 3, 4], 15, true)
  player.animations.add('right', [5, 6, 7, 8, 9], 15, true)
  player.frame = 1

  game.physics.enable(player, Phaser.Physics.ARCADE)
  game.physics.setBoundsToWorld()

  player.body.collideWorldBounds = true

  cursors = game.input.keyboard.createCursorKeys()
  game.input.keyboard.addKeyCapture([ Phaser.Keyboard.SPACEBAR ])

}

function update() {

  player.move()
  player.interact()

}
