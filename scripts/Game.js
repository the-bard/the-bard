const GAME_WIDTH = 1280
const GAME_HEIGHT = 720

const PLAYER_WIDTH = 70
const PLAYER_HEIGHT = 128

const PLAYER_SPAWN_X = GAME_WIDTH / 2 - PLAYER_WIDTH / 2
const PLAYER_SPAWN_Y = GAME_HEIGHT / 2 - PLAYER_HEIGHT / 2

const PLAYER_SPEED = 200

// --------------------------------------------------------------------------

var Bard = {}

Bard.Game = function(game) {
  this._player = null
  this._cursors = null
  Player._lastDirection = 'left'
}

Bard.Game.prototype = {

  preload: function() {
    this.load.spritesheet('_player', '../images/player.png', PLAYER_WIDTH, PLAYER_HEIGHT)
  },

  create: function() {

    this.world.bounds.setTo(0, 0, GAME_WIDTH, GAME_HEIGHT)

    this.stage.backgroundColor = '#ddd'

    this.physics.startSystem(Phaser.Physics.ARCADE)

    _player = new Player(game, PLAYER_SPAWN_X, PLAYER_SPAWN_Y)
    this.add.existing(_player)
    _player.animations.add('left', [0, 1, 2, 3, 4], 15, true)
    _player.animations.add('right', [5, 6, 7, 8, 9], 15, true)
    _player.frame = 1

    this.physics.enable(_player, Phaser.Physics.ARCADE)
    this.physics.setBoundsToWorld()

    _player.body.collideWorldBounds = true

    _cursors = this.input.keyboard.createCursorKeys()
    this.input.keyboard.addKeyCapture([ Phaser.Keyboard.SPACEBAR ])

  },

  update: function() {

    _player.move()
    _player.interact()

  }

}

// --------------------------------------------------------------------------

Player = function (game, x, y) {

    Phaser.Sprite.call(this, game, x, y, '_player')

}

Player.prototype = Object.create(Phaser.Sprite.prototype)
Player.prototype.constructor = Player

Player.prototype.move = function() {

  this.body.velocity.x = 0
  this.body.velocity.y = 0

  if (_cursors.left.isDown && !_cursors.right.isDown) {
    this.body.velocity.x = -PLAYER_SPEED
    Player.lastDirection = 'left'
  }
  else if (_cursors.right.isDown && !_cursors.left.isDown) {
    this.body.velocity.x = PLAYER_SPEED
    Player.lastDirection = 'right'
  }

  if (_cursors.up.isDown && !_cursors.down.isDown) {
    this.body.velocity.y = -PLAYER_SPEED
  }
  else if (_cursors.down.isDown && !_cursors.up.isDown) {
    this.body.velocity.y = PLAYER_SPEED
  }

  if (_cursors.left.isDown || _cursors.right.isDown ||
    _cursors.up.isDown || _cursors.down.isDown) {
      if (Player.lastDirection == 'left') {
        this.animations.play('left')
      }
      else {
        this.animations.play('right')
      }
  }

  if ((!_cursors.down.isDown || _cursors.up.isDown) &&
    (_cursors.down.isDown || !_cursors.up.isDown) &&
    (!_cursors.left.isDown || _cursors.right.isDown) &&
    (_cursors.left.isDown || !_cursors.right.isDown)) {
      this.animations.stop()
      if (Player.lastDirection == 'left') {
        this.frame = 1
      }
      else {
        this.frame = 6
      }
  }

}

Player.prototype.interact = function() {

  if (this.game.input.keyboard.downDuration(Phaser.Keyboard.SPACEBAR, 1)) {
    console.log('You pressed the spacebar!')
  }

}
