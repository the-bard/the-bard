Player = function (game, x, y) {

    Phaser.Sprite.call(this, game, x, y, 'player')

}

Player.prototype = Object.create(Phaser.Sprite.prototype)
Player.prototype.constructor = Player

var lastDirection = 'left'

Player.prototype.move = function() {

  this.body.velocity.x = 0
  this.body.velocity.y = 0

  if (cursors.left.isDown && !cursors.right.isDown) {
    this.body.velocity.x = -PLAYER_SPEED
    lastDirection = 'left'
  }
  else if (cursors.right.isDown && !cursors.left.isDown) {
    this.body.velocity.x = PLAYER_SPEED
    lastDirection = 'right'
  }

  if (cursors.up.isDown && !cursors.down.isDown) {
    this.body.velocity.y = -PLAYER_SPEED
  }
  else if (cursors.down.isDown && !cursors.up.isDown) {
    this.body.velocity.y = PLAYER_SPEED
  }

  if (cursors.left.isDown || cursors.right.isDown ||
    cursors.up.isDown || cursors.down.isDown) {
      if (lastDirection == 'left') {
        this.animations.play('left')
      }
      else {
        this.animations.play('right')
      }
  }

  if ((!cursors.down.isDown || cursors.up.isDown) &&
    (cursors.down.isDown || !cursors.up.isDown) &&
    (!cursors.left.isDown || cursors.right.isDown) &&
    (cursors.left.isDown || !cursors.right.isDown)) {
      this.animations.stop()
      if (lastDirection == 'left') {
        this.frame = 1
      }
      else {
        this.frame = 6
      }
  }

}

Player.prototype.interact = function() {

  if (game.input.keyboard.downDuration(Phaser.Keyboard.SPACEBAR, 1)) {
    console.log('You pressed the spacebar!')
  }

}
